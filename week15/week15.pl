
use strict;
use warnings;

my $filename = $ARGV[0];

open IN, '<', $filename or die "Cannot open $filename!\n";

my @lines = <IN>;

close IN;

foreach my $line (@lines) {
	chomp $line;
	if($line =~ /^(.*)\s+\-\s\-\s(\[.*\])\s+\".*\"\s+(\d{3})\s+(\d+)/){
		my $client = $1;
	
		my $time = $2;
	
		my $status = $3;
	
		my $dataSize = $4;
	
		print "Client:\t" . $client . " Time:\t". $time . " StatusCode:\t". $status . " DataSize:\t". $dataSize ."\n";
	}
}


