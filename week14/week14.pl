use strict;
use warnings;

print "type q or quit to exit\n";
print "Enter your input:";
my $input = <>;
chomp $input;#bu zımbırtı "\n"i siliyo

my $regex = '^\s*$';#match if string contains 0 or more white space characters
$regex = '^[A-Z]+$';#string contains all capital letters(at least one)
$regex = '[A-Z]\d*';#string contains a capital letter followed by 0 or more digits
$regex = '^\d+\.\d+$';#number $n contains some digits before and after a decimal points
$regex = '^(\d{1,3}\.){3}\d{1,3}$';#catch ip4 addres
until(($input eq "quit")||($input eq "q")) {
	if($input =~ /$regex/) { print "ACCEPTED\n"; }
	else { print "FAILED\n";}
	print "type q or quit to exit\n";
	print "Enter your input:";
	$input = <>;
	chomp $input;
}

